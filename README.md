# Metatrader 4 Collection (MT4, mq4)

## Disclaimer of Warranty

ALL THE COMPUTER PROGRAMS AND SOFTWARE ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND. WE MAKE NO WARRANTIES, EXPRESS OR IMPLIED, THAT THEY ARE FREE OF ERROR, OR ARE CONSISTENT WITH ANY PARTICULAR STANDARD OF MERCHANTABILITY, OR THAT THEY WILL MEET YOUR REQUIREMENTS FOR ANY PARTICULAR APPLICATION. THEY SHOULD NOT BE RELIED ON FOR SOLVING A PROBLEM WHOSE INCORRECT SOLUTION COULD RESULT IN INJURY TO A PERSON OR LOSS OF PROPERTY. IF YOU DO USE THEM IN SUCH A MANNER, IT IS AT YOUR OWN RISK. THE AUTHOR AND PUBLISHER DISCLAIM ALL LIABILITY FOR DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES RESULTING FROM YOUR USE OF THE PROGRAMS.

## Setup:
Just download the ZIP file and put the files in the corresponding Metatrader Folders.


## Buy / Sell scripts with automatic Stop Loss / Take Profit
![Buy/Sell Script Screenshot](images/BuySell.png)

## Support and Resistance Lines with configurable parameters
Drag and drop the script onto the chart!
![S&R Script Screenshot](images/SRLines.png)

## Credits / Thanks to:

https://www.mql5.com/en/code/12418

https://www.mql5.com/en/code/11162

http://www.forexfactory.com/showthread.php?p=4012790

http://www.forexfactory.com/showthread.php?t=193727

http://www.forexfactory.com/showthread.php?p=2723802#post2723802

## Did you like any of that?
[![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg)](https://www.paypal.me/SalvatoreBrundo)
