//+------------------------------------------------------------------+
//|                                                          Buy.mq4 |
//|                                                          salvixz |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "salvix"
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include "..\libraries\mine.mq4"

// #property show_inputs

extern double Lots=0.01;
extern bool   UseMoneyMgmt= false;
extern double RiskPercent = 2;
extern bool   UseStop=true;
extern bool   UseTakeProfit=true;
extern double StopLoss=200;
extern double TakeProfit=400;
extern string Note="0 in Entry field means Market Order Buy";
extern double Entry=0.0000;

//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
int OnStart()
{ 
   int order = execute_and_print(
      "BUY", 
      Lots, 
      Entry, 
      UseMoneyMgmt, 
      RiskPercent, 
      UseStop, 
      UseTakeProfit, 
      StopLoss, 
      TakeProfit
   );
   
   return order;
}
//+------------------------------------------------------------------+

