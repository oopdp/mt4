//+------------------------------------------------------------------+
//|                                               HelloWorld.mq4.mq4 |
//|                                                          salvixz |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "salvix"
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
#property show_inputs

extern int int_i = 1;

//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart()
{
   int conf;
   Comment("Here is the thing!");
   conf = MessageBox("Are you sure you want to proceed?\n\n Again");
   if(conf == 1){
      Print("OK!");
   }
   else {
      Print("Forget it!");
   }
}
//+------------------------------------------------------------------+
