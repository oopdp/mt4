//+------------------------------------------------------------------+
//|                                                      SR_Weak.mq4 |
//|                                                           salvix |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Random Email"
#property link      "https://gitlab.com/oopdp/mt4"
#property version   "1.00"

#include "..\libraries\mine.mq4"
short c_line_style = STYLE_DASH;
short c_line_width = 0;
color c_line_color = clrOrange;
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart()
{
   double price = ChartPriceOnDropped();
   datetime dt = ChartTimeOnDropped();
   printPriceLine((string)rand(), price, c_line_color, c_line_style, c_line_width);
}
//+------------------------------------------------------------------+
