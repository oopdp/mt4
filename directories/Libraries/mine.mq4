//+------------------------------------------------------------------+
//|                                                         mine.mq4 |
//|                                                           salvix |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property library
#property copyright "salvix"
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

//+------------------------------------------------------------------+
//| My function                                                      |
//+------------------------------------------------------------------+
// int MyCalculator(int value,int value2) export
//   {
//    return(value+value2);
//   }
//+------------------------------------------------------------------+


void printArrowUp(string id, datetime time, double price, color obj_color = Green)
{
   printArrow(id, SYMBOL_ARROWUP, time, price, obj_color);
}


void printArrowDown(string id, datetime time, double price, color obj_color = Red)
{
   printArrow(id, SYMBOL_ARROWDOWN, time, price, obj_color);
}


void printArrow(string id, int arrow_type, datetime time, double price, color obj_color = Red)
{
   Print(arrow_type);
   ObjectCreate(id, OBJ_ARROW, 0, time, price); //draw an up arrow
   ObjectSet(id, OBJPROP_STYLE, STYLE_SOLID);
   ObjectSet(id, OBJPROP_ARROWCODE, arrow_type);
   ObjectSet(id, OBJPROP_COLOR, obj_color);
   ObjectSet(id, OBJPROP_WIDTH, 3);
}

void printPriceLine(string id, double price, color line_color = Red, short line_style = STYLE_SOLID, short line_width = 0)
{
   ObjectCreate(id, OBJ_HLINE, 0, 0, price);
   ObjectSet(id, OBJPROP_STYLE, line_style);
   ObjectSet(id, OBJPROP_COLOR, line_color);
   ObjectSet(id, OBJPROP_WIDTH, line_width);
}


void printTimeLine(string id, datetime time, color line_color = Yellow, short line_style = STYLE_SOLID, short line_width = 0)
{
   ObjectCreate(id, OBJ_VLINE, 0, time, 0);
   ObjectSet(id, OBJPROP_STYLE, line_style);
   ObjectSet(id, OBJPROP_COLOR, line_color);
   ObjectSet(id, OBJPROP_WIDTH, line_width);
}

int buy(
   double Lots = 0.01,
   double Entry = 0.0000,
   bool UseMoneyMgmt = false, 
   double RiskPercent = 2, 
   bool UseStop = true, 
   bool UseTakeProfit = true, 
   double StopLoss = 200, 
   double TakeProfit = 400
)
{
   double Risk=RiskPercent/100;
   if(UseMoneyMgmt)
      Lots=NormalizeDouble(AccountBalance()*Risk/StopLoss/(MarketInfo(Symbol(),MODE_TICKVALUE)),2);
   int Mode=OP_BUYSTOP;
   if(Ask>Entry && Entry>0) Mode=OP_BUYLIMIT;
   if(Entry==0) {Entry=Ask; Mode=OP_BUY;}
   double SLB = Entry - StopLoss*Point, TPB = Entry + TakeProfit*Point;
   if(UseStop == false) SLB = 0;
   if(UseTakeProfit==false) TPB=0;
   return OrderSend(Symbol(),Mode,Lots,Entry,2,SLB,TPB,"Buy Script",0,NULL,LimeGreen);
}

int sell(
   double Lots = 0.01,
   double Entry = 0.0000,
   bool UseMoneyMgmt = false, 
   double RiskPercent = 2, 
   bool UseStop = true, 
   bool UseTakeProfit = true, 
   double StopLoss = 200, 
   double TakeProfit = 400 
)
{
   double Risk = RiskPercent / 100;
   if (UseMoneyMgmt)
      Lots = NormalizeDouble( AccountBalance()*Risk/StopLoss/(MarketInfo(Symbol(), MODE_TICKVALUE)),2); 
   int Mode = OP_SELLSTOP;
   if (Bid < Entry && Entry > 0) Mode = OP_SELLLIMIT; 
   if (Entry == 0)  {Entry = Bid; Mode = OP_SELL;}
   double  SLS = Entry+StopLoss*Point, TPS = Entry - TakeProfit * Point;
   if (UseStop == false) SLS = 0;
   if (UseTakeProfit == false) TPS = 0;
   return OrderSend(Symbol(),Mode, Lots, Entry, 2, SLS, TPS, "Sell Script",0, NULL, Red);
}


int execute_and_print(
   string OpType,
   double Lots = 0.01,
   double Entry = 0.0000,
   bool UseMoneyMgmt = false, 
   double RiskPercent = 2, 
   bool UseStop = true, 
   bool UseTakeProfit = true, 
   double StopLoss = 200, 
   double TakeProfit = 400
)
{
   int order = 0;
   
   if(OpType == "SELL"){
      order = sell(Lots, Entry, UseMoneyMgmt, RiskPercent, UseStop, UseTakeProfit, StopLoss, TakeProfit);
   }
   else if(OpType == "BUY") {
      order = buy(Lots, Entry, UseMoneyMgmt, RiskPercent, UseStop, UseTakeProfit, StopLoss, TakeProfit);
   }
   
   if(order > 0){
      bool orderSelect = OrderSelect(order, SELECT_BY_TICKET);
      if(orderSelect){
         string orderNo = IntegerToString(order);
         double orderPrice = OrderOpenPrice();
         datetime orderTime = OrderOpenTime();
         printPriceLine(orderNo+" SL", OrderStopLoss(), Red);
         printPriceLine(orderNo+" TP", OrderTakeProfit(), Green);
         printTimeLine(orderNo+" ENTRY TIME ", orderTime);
         printPriceLine(orderNo+" ENTRY PRICE", orderPrice, Yellow);
         return order;
      }
      else {
         Print("There was an error retrieving the order!");
      }
   }
   else {
      Print("There was an error placing the order!");
   }
   return order;
}