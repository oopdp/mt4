//+------------------------------------------------------------------+
//| Magnified Market Price.mq4        ver1.0             by Habeeb   |
//+------------------------------------------------------------------+

#property indicator_chart_window

  extern color  clrs = White;
  extern int    Label_Size=60;

int init()
  {
   return(0);
  }

int deinit()
  {
  ObjectDelete("Market_Price_Label"); 
  
  return(0);
  }

int start()
  {
   string Market_Price = DoubleToStr(Bid, 4);
  
   ObjectCreate("Market_Price_Label", OBJ_LABEL, 0, 0, 0);
   ObjectSetText("Market_Price_Label", Market_Price, Label_Size, "Arial", clrs);
  }