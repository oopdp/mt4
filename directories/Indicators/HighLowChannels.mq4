//+------------------------------------------------------------------+
//|                                                            z.mq4 |
//|                                                           salvix |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Random Email"
#property link      "https://gitlab.com/oopdp/mt4"
#property version   "1.00"
#property strict
#property indicator_chart_window
#property indicator_buffers 2
#property indicator_plots   2
//--- plot Label1
#property indicator_label1  "Lows"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrRed
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
//--- plot Label2
#property indicator_label2  "Highs"
#property indicator_type2   DRAW_LINE
#property indicator_color2  clrGreen
#property indicator_style2  STYLE_SOLID
#property indicator_width2  1
//--- indicator buffers
double         Label1Buffer[];
double         Label2Buffer[];
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
{
//--- indicator buffers mapping
   SetIndexBuffer(0, Label1Buffer);
   SetIndexBuffer(1, Label2Buffer);
   
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
      int i, due, done_so_far;
      due = rates_total - prev_calculated;
      // Print("TOT ", rates_total, " PREV ", prev_calculated, " DUE ", due);
      for(i = 0; i < due; i++ ) {        
         Label1Buffer[i] = low[i];
         Label2Buffer[i] = high[i];
      }
      done_so_far = prev_calculated + i;
      // Print("SO FAR DONE ", done_so_far);
      return(done_so_far);
}
//+------------------------------------------------------------------+
