//+------------------------------------------------------------------+
//|                                                            z.mq4 |
//|                                                           salvix |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Random Email"
#property link      "https://gitlab.com/oopdp/mt4"
#property version   "1.00"

// 
#include "..\libraries\mine.mq4"

// http://www.forexfactory.com/ffcal_week_this.xml
// https://www.dailyfx.com/files/Calendar-07-31-2016.csv
#property strict
#property indicator_chart_window
#property indicator_buffers 4
#property indicator_plots   4
//--- plot Label1
#property indicator_label1  "Buy Signals"
#property indicator_type1   DRAW_ARROW
#property indicator_color1  clrGreen
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
//--- plot Label2
#property indicator_label2  "Sell Signals"
#property indicator_type2   DRAW_ARROW
#property indicator_color2  clrRed
#property indicator_style2  STYLE_SOLID
#property indicator_width2  1
//--- plot Label3
#property indicator_label3  "Trend"
#property indicator_type3   DRAW_LINE
#property indicator_color3  clrGray
#property indicator_style3  STYLE_DOT
#property indicator_width3  1
//--- plot Label4
#property indicator_label4  "Triggers"
#property indicator_type4   DRAW_ARROW
#property indicator_color4  clrYellow
#property indicator_style4  STYLE_SOLID
#property indicator_width4  2

int arrow_down = 234;
int arrow_up = 233;
int trigger = 159;
int mininimum_candles_reqd = 10;
string indicatorName = "CandlesticKiller";
//--- indicator buffers
double BuySignals[];
double SellSignals[];
double Trend[];
double Triggers[];

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
{   
   IndicatorShortName(indicatorName);
   ArrayInitialize(BuySignals, EMPTY_VALUE);
   ArrayInitialize(SellSignals, EMPTY_VALUE);
   ArrayInitialize(Trend, EMPTY_VALUE);
   ArrayInitialize(Triggers, EMPTY_VALUE);  
   
   
   SetIndexBuffer(0, BuySignals);
   SetIndexArrow(0, arrow_up);
   
   SetIndexBuffer(1, SellSignals);
   SetIndexArrow(1, arrow_down);
   
   SetIndexBuffer(2, Trend);
   
   SetIndexBuffer(3, Triggers);
   SetIndexArrow(3, trigger);
   
   return(INIT_SUCCEEDED);
}

void OnDeinit(const int reason)
{
   ObjectsDeleteAll(0, OBJ_TEXT); // wrong
}
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
      int i = 0;
      int due = rates_total - prev_calculated;
      int limit = MathMin(due, (rates_total - mininimum_candles_reqd));
      int done_so_far = 0;
      //Print("GOT ENOUGH CANDLES.....");
      //Print("TOT ", rates_total, " PREV ", prev_calculated, " DUE ", due, " BUT DOING LIMIT ", limit);
      double
      O0, O1, O2, O3, O4, 
      H0, H1, H2, H3, H4,
      C0, C1, C2, C3, C4, 
      L0, L1, L2, L3, L4; 
      
      for(i = 0; i < limit; i++ ) {        
      
         Trend[i] = (low[i] + high[i]) / 2;
         
         O0 = open[i];
         O1 = open[i+1];
         O2 = open[i+2];
         O3 = open[i+3];
         O4 = open[i+4];            
         
         C0 = close[i];
         C1 = close[i+1];
         C2 = close[i+2];
         C3 = close[i+3];
         C4 = close[i+4];
         
         L0 = low[i];
         L1 = low[i+1];
         L2 = low[i+2];
         L3 = low[i+3];
         L4 = low[i+3];
         
         H0 = high[i];
         H1 = high[i+1];
         H2 = high[i+2];
         H3 = high[i+3];
         H4 = high[i+3];
         
         
         string currentCandlType = oopdp_identifyCandleType(O0, H0, C0, L0);
         if(currentCandlType == "DOJI"){
            // oopdp_printLabel("CK_"+(string)rand(), "DOJI", time[i], high[i] + 50 * Point); // not very good idea...
            // BuySignals[i] = high[i];
            // Triggers[i] = high[i];
            
            Triggers[i] = high[i];
         }          
         
      }
      
      // this forces the 2nd iteration to kill 
      // the arrears, after that though 
      // the loop runs clean only on new candles
      done_so_far = prev_calculated + i; 
      // Print("SO FAR DONE ", done_so_far);
      return(done_so_far);
}


void oopdp_printLabel(string id, string label, datetime time, double price, color clr = clrGray, int size = 6)
{
   ObjectCreate(id, OBJ_TEXT, 0, time, price);
   // ObjectSet(id, OBJPROP_COLOR, clr);
   // ObjectSet(id, OBJPROP_ANGLE, 90);   //vertical but not very good centering...
   ObjectSetText(id, label, size);
}
//+------------------------------------------------------------------+
string oopdp_getStringCurrentPeriod()
{
   string period;
   switch (Period()) {
      case 1:
         period = "M1";
      break;
      case 5:
         period = "M5";
      break;
      case 15:
         period = "M15";
      break;
      case 30:
         period = "M30";
      break;      
      case 60:
         period = "H1";
      break;
      case 240:
         period = "H4";
      break;
      case 1440:
         period = "D1";      
      break;
      case 10080:
         period = "W1";
      break;
      case 43200:
         period = "MN";
      break;
   }
   return period;
}

int oopdp_getCurrentPeriod()
{
   return Period();
}

string oopdp_identifyCandleType(double O, double H, double C, double L){
   
   // Reference params:
   double dojiRatio = 5;
   double minRange = (1) * Point;
   
   // calculations:
   double range = MathAbs(H - L);
   if(range < minRange){ // skip rubbish
      return "NA";
   }
   double body = MathAbs(C - O);
   double bodyRangeRatio = (body / range) * 100;
   
   
   if(bodyRangeRatio < dojiRatio){
      return "DOJI";
   }
  
   return "NA";
}